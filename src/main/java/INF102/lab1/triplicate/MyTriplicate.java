package INF102.lab1.triplicate;

import java.util.List;
import java.util.HashMap;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        HashMap<T,Integer> triplicates = new HashMap<T,Integer>();
        for (T item : list) {
            if (triplicates.containsKey(item)){
                triplicates.put(item, triplicates.get(item)+1);
                if (triplicates.get(item) == 3) {
                    return item;
                }
            } else {
                triplicates.put(item,1);
            }
        }
        return null;
    }
    
}
